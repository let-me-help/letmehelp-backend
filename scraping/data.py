import requests
import json
from event import *
from charity import *
from city import *
import boto3
import sys
import decimal
from boto3.dynamodb.conditions import Key, Attr
import datetime


client = boto3.client('dynamodb')
resource = boto3.resource('dynamodb', region_name='us-west-2')
Charities_table = resource.Table('Charities')
Events_table = resource.Table('Events')
cities_table = resource.Table('Cities')

# input = [['Austin', 'Texas'],['Chicago', 'Illinois'], ['New York', 'New York'], 
#         ['Los Angeles', 'California'], ['Houston', 'Texas'], ['Phoenix', 'Arizona'],
#         ['Philadelphia', 'Pennsylvania'],['Dallas', 'Texas'], ['Jacksonville', 'Florida'], 
#         ['San Diego', 'California'], ['San Antonio', 'Texas']]
input = [['San Jose', 'California'], ['San Francisco','California'],['Columbus', 'Ohio'],['Fort Worth', 'Texas'],
        ['Indianapolis', 'Indiana'],['Charlotte', 'North Carolina'], ['Seattle', 'Washington'],
        ['Denver', 'Colorado'],['Washington', 'District of Columbia'], ['Boston', 'Massachusetts']]

lookupIds = {'Austin' : '16000US4805000', 'New York': '16000US3651000', 'Chicago': '16000US1714000', 
             'Los Angeles' : '16000US0644000', 'Houston' :'16000US4835000', 'Phoenix': '16000US0455000',
             'Philadelphia': '16000US4260000', 'San Antonio': '16000US4865000', 'San Diego': '16000US0666000',
             'Dallas' : '16000US4819000', 'San Jose' : '16000US0668000', 'Jacksonville' : '16000US1235000',
             'San Francisco' : '16000US0667000', 'Columbus' : '16000US3918000', 'Fort Worth' : '16000US4827000',
             'Indianapolis' : '16000US1836003', 'Charlotte' : '16000US3712000', 'Seattle' : '16000US5363000',
             'Denver': '16000US0820000', 'Washington' : '16000US1150000', 'Boston' : '16000US2507000'}

for city,state in input:
    print("For: " + city)

    print("Getting Events")
    RawEvents = getAllEvents(city)
    EventList, OrganizationIdList = scrapEvent(RawEvents, city, state)

    print("Getting Orgs")
    allOrgList, NotOrgList = getAllOrgs(OrganizationIdList, city, state)
    
    print("Getting City Data")
    newCity = fillCityInfo(city, state, lookupIds[city])
    
    print("Adding Events for " + city)
    t = 1
    for item in EventList:
        if item.organizationId not in NotOrgList:
            t = t + 1
            Events_table.put_item(Item= item.__dict__)
    print("Events Added " + str(t))

    print("Adding Orgs for " + city)
    t = 1
    for item in allOrgList:
        t = t + 1
        Charities_table.put_item(Item= item.__dict__)
    print("Orgs Added " + str(t))

    print('Adding that to Dynamo DB')
    cities_table.put_item(Item= newCity.__dict__)



print("------------Done----------")