import requests
import json

authHeader = {"Authorization": Authorization,}

class event:
    def __init__(self, name, descriptionHTML, descriptionText, eventId, eventUrl, startTime, endTime, organizationId, venueId, logoUrl):
        self.name = name
        self.descriptionHTML = descriptionHTML
        self.descriptionText = descriptionText
        self.eventId = eventId
        self.eventUrl = eventUrl
        self.startTime = startTime
        self.endTime = endTime
        self.organizationId = organizationId
        self.venueId = venueId
        self.logoUrl = logoUrl
        #From Venue EndPoint
        self.latitude = ''
        self.address_city = ''
        self.longitude = ''
        self.address_1 = ''
        self.address_2 = ''
        self.city = ''
        self.state = ''
        self.zipcode = ''
        self.localized_address_display = ''


def getAllEvents(city):
    allEvents = []
    has_more = True
    page = 1
    limit  = 2

    while has_more:
        response = requests.get("https://www.eventbriteapi.com/v3/events/search?location.address=" + city + "&categories=111&page=" + str(page),
        headers = authHeader,
        verify = True,  # Verify SSL certificate
        )
        responseJson = response.json()

        
        allEvents.extend(responseJson['events'])

        if responseJson['pagination']['has_more_items'] == False:
            has_more = False    
        page += 1

        if page > limit:
            break

    return allEvents

def scrapEvent(allEvents, area, state):
    EventList = []
    OrganizationIdList = []

    for itemEvent in allEvents:

        #Make sure the event has logo
        if itemEvent['logo']:
            logo = itemEvent['logo']['url']
        else:
            logo = "https://s3-us-west-2.amazonaws.com/public.letmehelp.life/download1.jpeg"

        #Create an event object
        newEvent =  event(itemEvent['name']['text'], itemEvent['description']['html'], itemEvent['description']['text'],
         itemEvent['id'], itemEvent['url'], itemEvent['start']['local'], itemEvent['end']['local'], itemEvent['organizer_id'],
         itemEvent['venue_id'], logo)
        
        response = requests.get("https://www.eventbriteapi.com/v3/venues/" + itemEvent['venue_id'],
        headers = authHeader,
        verify = True,  # Verify SSL certificate
        )
        
        responseJson = response.json()
        newEvent.city = area
        if 'address' in responseJson:
            newEvent.address_1 = responseJson['address']['address_1']
            newEvent.address_2 = responseJson['address']['address_2']
            newEvent.address_city = responseJson['address']['city']
            newEvent.state = state
            newEvent.zipcode = responseJson['address']['postal_code']
            newEvent.latitude = responseJson['address']['latitude']
            newEvent.longitude = responseJson['address']['longitude']
            newEvent.localized_address_display = responseJson['address']['localized_address_display']

        EventList.append(newEvent)
        if newEvent.organizationId not in OrganizationIdList:
            OrganizationIdList.append(newEvent.organizationId)


    return EventList, OrganizationIdList



    
    

