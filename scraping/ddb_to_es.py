
import json
import boto3
from elasticsearch import Elasticsearch, RequestsHttpConnection
from elasticsearch.helpers import bulk
from requests_aws4auth import AWS4Auth
from time import clock

def connect():
    session = boto3.session.Session()
    credentials = session.get_credentials()

    awsauth = AWS4Auth(credentials.access_key,
                    credentials.secret_key,
                    session.region_name, 'es',
                    session_token=credentials.token)

    es = Elasticsearch(
        ["https://search-letmehelp-x7kx7zqnvvb6fpqg3k4jlclrgi.us-west-2.es.amazonaws.com"],
        http_auth=awsauth,  
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection
    )
    return es

def ddb_stream(table, table_name):
    result = table.scan()

    while(result):
        for item in result["Items"]:
            yield   {
                "_index": table_name.lower(),
                "_type": table_name.lower(),
                "_source": item
            }
        if "LastEvaluatedKey" in result:
            result = table.scan(ExclusiveStartKey=result["LastEvaluatedKey"])
        else:
            result = None
    print("All dynamo items taken care of")
    print("Double check error message for problems with retrieving items")

def upload_ddb_to_es(es, ddb_resource):
    tables = ["Charities", "Events", "Cities"]

    for table_name in tables:
        table = ddb_resource.Table(table_name)
        stream = ddb_stream(table, table_name)
        result = None
        try:
            result = bulk(es, stream)
        except Exception as e:
            print(e)
        print(result[0])


def build_keyword_mapping(index):
    keywords = {}
    with open(f'{index}_mappings.json') as mf:
        properties = json.load(mf)['mappings']['_doc']['properties']

        for property in properties:
            if properties[property]["type"] == "text":
                keywords[property] = f"{property}.keyword"

    print(json.dumps(keywords, indent=4))

def empty_es(es):
    indices = ['charities', 'cities', 'events']

    for index in indices:
        es.indices.delete(index=index)
        print("Deleted: " + index)
        with open(f"{index}_mappings.json") as mf:
            es.indices.create(index=index, body=json.load(mf))
            print("Created: " + index)


if __name__ == "__main__":
    ddb_resource = boto3.resource("dynamodb")

    es = connect()
    empty_es(es)
    upload_ddb_to_es(es, ddb_resource)

