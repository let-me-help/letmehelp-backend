import requests
import json

authHeader = {"Authorization": Authorization,}

class charity:
    def __init__(self, name, organizationId, descriptionHTML, descriptionText, logoUrl, city, state):
        self.name = name
        self.descriptionHTML = descriptionHTML
        self.descriptionText = descriptionText
        self.organizationId = organizationId
        self.logoUrl = logoUrl
        self.city = city
        self.state = state
        self.socialMedia = None
        #
        self.charityWebsite = None
        self.cause = None
        self.donationUrl = None
        self.latitude = None
        self.longitude = None
        #self.address = None
        self.zipCode = None

def getAllOrgs(organizations, city, state):
    OrgsList = []
    NotOrgList = []
    for orgId in organizations:
        first_response = requests.get("https://www.eventbriteapi.com/v3/organizers/" + orgId,
                                        headers = authHeader,
                                        verify = True,  # Verify SSL certificate
                                        )
        
        charityJson = first_response.json()
        
        logo = None
        if 'logo' in charityJson:
            if charityJson['logo']:
                logo = charityJson['logo']['url']
        
        if 'name' not in charityJson or charityJson['name'] == None:
            NotOrgList.append(orgId)
            continue
            

        NewCharity =  charity(charityJson['name'], orgId, charityJson['long_description']['html'], charityJson['long_description']['text'], logo, city, state)

        socialMedia = dict()
        if 'facebook' in charityJson:
            socialMedia['facebook'] = charityJson['facebook']
        
        if 'instagram' in charityJson:
            socialMedia['instagram'] = charityJson['instagram']

        if 'twitter' in charityJson:
            socialMedia['twitter'] = charityJson['twitter']

        NewCharity.socialMedia = socialMedia
        
        second_response = requests.get("http://data.orghunter.com/v1/charitysearch?user_key=user_key&searchTerm=" + NewCharity.name + "&State=" + state,)
        charityJson1 = second_response.json()

        data = charityJson1['data']

        if data:
            orgInfo = data[0]
            if orgInfo['donationUrl'] != '':
                NewCharity.donationUrl = orgInfo['donationUrl']
            else:
                NewCharity.donationUrl = None
            
            if orgInfo['website'] != '':
                NewCharity.charityWebsite = orgInfo['website']
            else:
                NewCharity.charityWebsite = 'https://www.google.com/search?q=' + NewCharity.name.replace(' ','+') + '+' + NewCharity.city
            
            if orgInfo['category'] != '':
                NewCharity.cause = orgInfo['category']
            else:
                NewCharity.cause = None
            
            if orgInfo['longitude'] != '':
                NewCharity.longitude = orgInfo['longitude']
            else:
                NewCharity.longitude = None
            
            if orgInfo['latitude'] != '':
                NewCharity.latitude = orgInfo['latitude']
            else:
                NewCharity.latitude = None
            
            if orgInfo['zipCode'] != '':
                NewCharity.zipCode = orgInfo['zipCode']
            else:
                NewCharity.zipCode = None

            if NewCharity.logoUrl == None:
                NewCharity.logoUrl = "https://s3-us-west-2.amazonaws.com/public.letmehelp.life/images1.jpeg"

            if NewCharity.descriptionText == None:
                NewCharity.descriptionText = NewCharity.name + " is a wonderful charitable organization, Please Consider Donating!!! To Learn more about " +  NewCharity.name + ", Please check out the orgnization website."

            OrgsList.append(NewCharity)
        else:
            NotOrgList.append(orgId)

    return OrgsList, NotOrgList

