import requests
import json


class city:
    def __init__(self, name, state):
        self.city_name = name
        self.state = state
        self.population = None
        self.income_below_poverty = None
        self.description = None
        self.children_in_poverty = None
        self.data_year = '2016'
# Need altitude and latitude 

def fillCityInfo(ct, state,  lookupId):
    newCity = city(ct, state)
    stats = {'pop' : '', 'income_below_poverty': '', 'children_in_poverty': ''}
    
    for stat in stats:
        response = requests.get("http://api.datausa.io/api/?show=geo&required=" + stat + "&year=2016&geo=" + str(lookupId),)
        responseJson = response.json()
        stats[stat] = responseJson['data'][0][2]
    newCity.population = str(stats['pop'])
    newCity.income_below_poverty = str(stats['income_below_poverty'])
    newCity.children_in_poverty = str(stats['children_in_poverty'])

    response = requests.get("https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=" + ct,)
    responseJson = response.json()
    pages =  responseJson['query']['pages']
    for item in pages:
        if 'extract' in pages[item]:
            newCity.description = pages[item]['extract']
    
    return newCity
