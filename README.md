Daniel Loera dl29576 danielloera  
Estimated completion time: 10hr  
Actual completion time: 5hr  

Jorge Hernandez jmh6892 jmhern  
Estimated completion time: 5hr  
Actual completion time: 5hr  

Patrick Klingler plk395 pklingler  
Estimated completion time: 10hr  
Actual completion time: 8hr  

Rahul Parikh rmp2798 Rahul.Parikh  
Estimated completion time:5hr  
Actual completion time: 3hr  

Rinchen Tsering rt24459 rinchman  
Estimated completion time: 8hr  
Actual completion time: 8hr  

Git SHA: 4430f2fe7ac0836eae98bc9867c3cf869e349a43

[Pipelines](https://gitlab.com/let-me-help/letmehelp.life/pipelines) 
 
Website: [letmehelp.life](http://letmehelp.life)  
RESTful API: [api.letmehelp.life](http://api.letmehelp.life)

## letmehelp-backend
Welcome to official repository for [letmehelp.life](http://letmehelp.life) back-end! 
Our mission is to provide an efficient, useful, and highly interactive website to discover charities near you.  

Use LetMeHelp to find the perfect charity for you, the easy way!

## Backend and Database Information
Unittests and Postman integration tests are located under `sam-app/tests`

The API/Backend is under `sam-app/letmehelp`

Database/API scraping is under `scraping`



Testing the backend requires ``boto3``, ``pytest``, ``pytest-mock``, ``elasticsearch``, and ``requests_aws4auth``

These can all be installed via ``pip``

The unittests can be run by running the following commands:

    cd sam-app
    pytest tests/


Refer to the sam-app readme for running the backend locally via SAM
