import json
import os

import boto3
from elasticsearch import Elasticsearch, RequestsHttpConnection
from elasticsearch.helpers import bulk
from requests_aws4auth import AWS4Auth

from datetime import datetime, timedelta

dynamodb_resource = boto3.resource("dynamodb")
dynamodb = None
es = None

html_characters = {"%3A": ":", "%20": " ", "%2C": ","}

def get_es_connection():
    session = boto3.session.Session()
    credentials = session.get_credentials()

    awsauth = AWS4Auth(credentials.access_key,
                    credentials.secret_key,
                    session.region_name, 'es',
                    session_token=credentials.token)

    return Elasticsearch(
        ["https://search-letmehelp-x7kx7zqnvvb6fpqg3k4jlclrgi.us-west-2.es.amazonaws.com"],
        http_auth=awsauth,  
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection
    )


def initialize_es(es_=None):
    global es
    if es_:
        es = es_
    elif not es:
        es = get_es_connection()


def initialize_dynamodb(dynamodb_=None):
    global dynamodb
    if dynamodb_:
        dynamodb = dynamodb_
    elif not dynamodb:
        dynamodb = dynamodb_resource.Table(os.getenv('TABLE_NAME'))


def success_response(msg) -> dict:
    return {
        "statusCode": 200,
        "body": json.dumps(msg),
        "headers": {"Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Credentials": True}
    }


def es_sort_params(sort_args, keywords):
    for keyword in keywords:
        sort_args = sort_args.replace(keyword, keywords[keyword])
    return sort_args


def get_es_range_queries(ranges, args):
    range_query = []
    range_args = {ranges[k]: v for k, v in args.items() if k in ranges}

    for arg in range_args:
        start, stop = None, None
        convert = float
        try:
            start, stop = range_args[arg].split('-')
        except ValueError:
            start, stop = range_args[arg].split('--')
            convert = str

        range_query.append({
            "range": {
                arg: {
                    "gte": convert(start),
                    "lte": convert(stop)
                }
            }
        })

    return range_query


def get_es_match_queries(matches, args):
    match_args = {matches[k]: v for k, v in args.items() if k in matches}
    return [{"match": {k:v}} for k,v in match_args.items()]


def get_es_query_string_query(args):
    if "q" in args:
        return [{
            "query_string": 
                {"query": args["q"].replace("%20", " ")}
        }]
    return []

def construct_es_query(args, ranges, matches):
    query = []

    if "date" in args:
        args['date'] = create_date_range(args['date'])

    query.extend(get_es_range_queries(ranges, args))
    query.extend(get_es_match_queries(matches, args))
    query.extend(get_es_query_string_query(args))

    if not query:
        query = {"match_all": {}}
    elif len(query) > 1:
        query = {"bool": {"must": query}}
    else:
        query = query[0]

    return {"query": query }


def get_path_to_resource_file(resource):
    return f"{os.environ['LAMBDA_TASK_ROOT']}/{resource}.json"


def replace_null(item, replacement=""):
    for attr in item:
        if not item[attr]:
            item[attr] = replacement
    return item


def format_es_result(result):
    return [replace_null(item['_source']) for item in result['hits']['hits']]


def get_resource_by_id(event, context):
    initialize_dynamodb()

    id_name, id_value = list(event['pathParameters'].items())[0]
    id_value = id_value.replace('%20', ' ')

    return success_response(
        replace_null(dynamodb.query(
            KeyConditionExpression=f"{id_name} = :id",
            ExpressionAttributeValues={":id": id_value}
        )["Items"][0])
    )


def create_date_range(start, days=1):
    start_ = start.replace("T", " ")
    end = datetime.strptime(start_[:-1],"%Y-%m-%d %H:%M:%S.%f")
    end = (end + timedelta(days=days)).strftime("%Y-%m-%d %H:%M:%S.%f")
    end = end.replace(" ", "T")
    end += "Z"
    return f"{start}--{end}"


def format_args(args):
    if args:
        for arg in args:
            val = args[arg]
            for char in html_characters:
                val = val.replace(char, html_characters[char])
        return args
    return {}


def search(event, context):
    initialize_es()
    args = event["queryStringParameters"] or {}
    args = format_args(args)
    
    resource_name = event['resource'][1:]

    offset = 0 if not "offset" in args else int(args["offset"])
    max_results = 40 if not "max_results" in args else int(args["max_results"])
    sort = "" if not "sort" in args else args['sort']

    with open(get_path_to_resource_file(event['resource'])) as mf:
        resource_params = json.load(mf)
        ranges, matches = resource_params['ranges'], resource_params['matches']
        
        return success_response(
            format_es_result(
                es.search(
                    index=resource_name,
                    body=construct_es_query(args, ranges, matches),
                    from_=offset,
                    size=max_results,
                    sort=es_sort_params(sort, resource_params['keywords'])
        )))
