from collections import namedtuple
import pytest
import json
import os

from letmehelp.app import *

@pytest.fixture()
def apigw_event():
    """ Generates API GW Event"""

    return {
        "body": '{ "test": "body"}',
        "resource": "/events",
        "requestContext": {
            "resourceId": "123456",
            "apiId": "1234567890",
            "resourcePath": "/{proxy+}",
            "httpMethod": "POST",
            "requestId": "c6af9ac6-7b61-11e6-9a41-93e8deadbeef",
            "accountId": "123456789012",
            "identity": {
                "apiKey": "",
                "userArn": "",
                "cognitoAuthenticationType": "",
                "caller": "",
                "userAgent": "Custom User Agent String",
                "user": "",
                "cognitoIdentityPoolId": "",
                "cognitoIdentityId": "",
                "cognitoAuthenticationProvider": "",
                "sourceIp": "127.0.0.1",
                "accountId": "",
            },
            "stage": "prod",
        },
        "queryStringParameters": {"id": "10"},
        "headers": {
            "Via": "1.1 08f323deadbeefa7af34d5feb414ce27.cloudfront.net (CloudFront)",
            "Accept-Language": "en-US,en;q=0.8",
            "CloudFront-Is-Desktop-Viewer": "true",
            "CloudFront-Is-SmartTV-Viewer": "false",
            "CloudFront-Is-Mobile-Viewer": "false",
            "X-Forwarded-For": "127.0.0.1, 127.0.0.2",
            "CloudFront-Viewer-Country": "US",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Upgrade-Insecure-Requests": "1",
            "X-Forwarded-Port": "443",
            "Host": "1234567890.execute-api.us-east-1.amazonaws.com",
            "X-Forwarded-Proto": "https",
            "X-Amz-Cf-Id": "aaaaaaaaaae3VYQb9jd-nvCd-de396Uhbp027Y2JvkCPNLmGJHqlaA==",
            "CloudFront-Is-Tablet-Viewer": "false",
            "Cache-Control": "max-age=0",
            "User-Agent": "Custom User Agent String",
            "CloudFront-Forwarded-Proto": "https",
            "Accept-Encoding": "gzip, deflate, sdch",
        },
        "pathParameters": {"proxy": "/examplepath"},
        "httpMethod": "GET",
        "stageVariables": {"baz": "qux"},
        "path": "/examplepath",
    }

def load_data(**kwargs):
    return {"statusCode": 200, "body": "{\"message\": \"true\"}"}

def load_data_db(**kwargs):
    return {"Items": [{"message": "hello world"}]}

# #@Author patrick
# def test_lambda_handler(apigw_event, mocker):
#     mock_ddb = mocker.MagicMock(name='ddb')
#     mock_ddb.get_item.side_effect = load_data_db

#     mock_ddb_query = mocker.MagicMock(name='ddb_query')
#     mock_ddb_query.get_response.side_effect = load_data

#     mocker.patch('letmehelp.dynamo_db_query.dynamodb', new=mock_ddb)
#     mocker.patch('letmehelp.app.ddb_query', new=mock_ddb_query)
#     mocker.patch.dict(os.environ, {"LAMBDA_TASK_ROOT": "."})

#     ret = lambda_handler(apigw_event, "")

#     assert ret["statusCode"] == 200

#     assert "message" in ret["body"]

#     data = json.loads(ret["body"])
#     assert data["message"] == "hello world"

def test_search(apigw_event, mocker):
    #TODO
    pass
