from letmehelp.app import *

import pytest
import json
import os

scan_query = {
    "FilterExpression": "contains(cause, :cause) and #st = :state",
    "ExpressionAttributeValues": {":state": "Texas", ":cause": "Education"},
    "ExpressionAttributeNames": {"#st": "state"}
}

id_query = {
    "Key": {"organizationId": "10"}
}

es_response = {
    "hits": {
        "hits": [
            {
                "_source": {
                "name": "Education charity",
                "id": "10",
                "state": "Texas",
                "city": "Austin",
                "website": None
                }
            },
            {
                "_source": {
                "name": "Education charity 2",
                "id": "11",
                "state": "Texas",
                "city": "Dallas",
                "website": "google.com"
                }
            }
        ]
    }
}

ddb_id_response = {
    "Items":[
        {
            "name": "Education charity",
            "id": "10",
            "state": "Texas",
            "city": "Austin",
            "website": None
        }
    ]
}

es_formatted_response = [
        {
            "name": "Education charity",
            "id": "10",
            "state": "Texas",
            "city": "Austin",
            "website": ""
        },
        {
            "name": "Education charity 2",
            "id": "11",
            "state": "Texas",
            "city": "Dallas",
            "website": "google.com"
        }
]

response = {
    "statusCode": 200,
    "body": json.dumps([
        {
            "name": "Education charity",
            "id": "10",
            "state": "Texas",
            "city": "Austin",
            "website": ""
        },
        {
            "name": "Education charity 2",
            "id": "11",
            "state": "Texas",
            "city": "Dallas",
            "website": "google.com"
        }
    ]),
    "headers": {"Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": True}
}

id_response = {
    "statusCode": 200,
    "body": json.dumps(
        {
            "name": "Education charity",
            "id": "10",
            "state": "Texas",
            "city": "Austin",
            "website": ""
        }),
    "headers": {"Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": True}
}

filter_request = {
    "resource": "/events",
    "queryStringParameters": {
        "state": "Texas"
    }
}

range_request = {
    "resource": "/cities",
    "queryStringParameters": {
        "population": "1000000-2000000"
    }
}

sort_request = {
    "resource": "/charities",
    "queryStringParameters": {
        "sort": "name"
    }
}

sort_request = {
    "resource": "/cities",
    "queryStringParameters": {
        "sort": "name,state:desc"
    }
}

multi_request = {
    "resource": "/cities",
    "queryStringParameters": {
        "population": "1000000-2000000",
        "state": "Texas",
        "sort": "population,city_name:desc"
    }
}


@pytest.fixture()
def args():
    return {
        "key1": "111-222",
        "key2": "val2",
        "key3": "val3",
        "key4": "2018-01-01--2018-01-02",
        "q": "foo"
    }

@pytest.fixture()
def ranges():
    return {
        "key1": "key1",
        "key4": "key4UTC"
    }

@pytest.fixture()
def terms():
    return {
        "key2": "key2.keyword",
        "key3": "key3"
    }

@pytest.fixture()
def ddb_item_null():
    return {"id": "1", "name": "Test", "website": None}


@pytest.fixture()
def ddb_item_empty():
    return {"id": "1", "name": "Test", "website": ""}

# # @Author patrick
# def test_get_response_id(query_id, mocker):

#     mock_ddb = mocker.MagicMock(name='ddb')
#     mock_ddb.get_item.side_effect = load_data
#     mocker.patch('letmehelp.dynamo_db_query.dynamodb', new=mock_ddb)

#     result = query_id.get_response()

#     assert result == id_response

# @Author patrick
def test_success_response():
    assert success_response("") == {
        "statusCode": 200,
        "body": '""',
        "headers": {"Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Credentials": True}
    }

# @Author patrick
def test_replace_null(ddb_item_null, ddb_item_empty):
    assert replace_null(ddb_item_null, "") == ddb_item_empty


#@Author patrick
def test_get_event_by_id(mocker):
    ddb_params = {"query.return_value": ddb_id_response}
    mocker.patch('letmehelp.app.dynamodb', **ddb_params)

    assert get_resource_by_id(
        {'pathParameters': 
            {"eventId": "111919111"}
        }, None) == id_response

#@Author patrick
def test_get_city_by_name(mocker):
    ddb_params = {"query.return_value": ddb_id_response}
    mocker.patch('letmehelp.app.dynamodb', **ddb_params)

    assert get_resource_by_id(
        {'pathParameters': 
            {"city_name": "El%20Paso"}
        }, None) == id_response

#@Author patrick
def test_get_path_to_resource_file(mocker):
    mocker.patch.dict('os.environ', {"LAMBDA_TASK_ROOT": "."})
    assert get_path_to_resource_file("file") == "./file.json"

#@Author patrick
def test_format_es_result():
    assert format_es_result(es_response) == es_formatted_response

#@Author patrick
def test_create_date_range():
    date = "2018-01-31T08:00:00.00Z"
    assert create_date_range(date) == "2018-01-31T08:00:00.00Z--2018-02-01T08:00:00.000000Z"

#@Author patrick
def test_create_date_range_5():
    date = "2018-01-31T08:00:00.00Z"
    assert create_date_range(date, 5) == "2018-01-31T08:00:00.00Z--2018-02-05T08:00:00.000000Z"

#@Author patrick
def test_format_args():
    args = {"key1": "value%2A1", "key2": "value%202", "key3": "value%2C3"}
    
    format_args(args) == {"key1": "value:1", "key2": "value 2", "key3": "value,3"}


#@Author patrick
def test_es_sort_params():
    keywords = {"key1":"val1", "key2":"val2"}
    sort = "key1,key2:desc"
    assert es_sort_params(sort, keywords) == "val1,val2:desc"


#@Author patrick
def test_es_range(args):
    args = {
        "key1":"111-222",
        "key2":".1-.2",
        "key3":"2018-01-01--2018-01-02"
    }

    ranges = {
        "key1": "key1",
        "key2": "key2",
        "key3": "key3"
    }

    assert get_es_range_queries(ranges, args) == [
        {"range": {
            "key1": {
                "gte": 111,
                "lte": 222
            }
        }},
        {"range": {
            "key2": {
                "gte": .1,
                "lte": .2
            }
        }},
        {"range": {
            "key3": {
                "gte": "2018-01-01",
                "lte": "2018-01-02"
            }
        }}]


#@Author patrick
def test_es_construct_query_compound(args, ranges, terms):
    query = construct_es_query(args, ranges, terms)
    assert "query" in query
    assert "bool" in query["query"]
    assert "must" in query["query"]["bool"]
    assert all(key in query["query"]["bool"]["must"] for key in [
        {"range": {"key1": {"gte":111, "lte":222}}},
        {"range": {"key4UTC": {"gte":"2018-01-01", "lte":"2018-01-02"}}},
        {"match": {"key2.keyword": "val2"}},
        {"match": {"key3": "val3"}},
        {"query_string": {"query": "foo"}}
    ])

#@Author patrick
def test_es_construct_query_all(ranges, terms):
    query = construct_es_query({}, ranges, terms)
    assert "query" in query
    assert "match_all" in query["query"]
    assert query["query"]["match_all"] == {}


#@Author patrick
def test_es_construct_query_match(args, ranges, terms):
    args = {"key2": args["key2"]}
    query = construct_es_query(args, ranges, terms)
    assert "query" in query
    assert "match" in query["query"]
    assert query["query"]["match"] == {"key2.keyword": "val2"}

#@Author patrick
def test_es_construct_query_range(args, ranges, terms):
    args = {"key1": args["key1"]}
    query = construct_es_query(args, ranges, terms)
    assert "query" in query
    assert "range" in query["query"]
    assert query["query"]["range"] == {"key1": {"gte": 111, "lte": 222}}


#@Author patrick
def test_es_construct_query_query_string(args, ranges, terms):
    args = {"q": args["q"]}
    query = construct_es_query(args, ranges, terms)
    assert "query" in query
    assert "query_string" in query["query"]
    assert "query" in query["query"]["query_string"]
    assert query["query"]["query_string"]["query"] == args["q"]

