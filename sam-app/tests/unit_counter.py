import glob
import boto3
from io import StringIO
from collections import Counter
import json

s3 = boto3.resource('s3')
bucket = s3.Bucket('letmehelp-gitlab-stats')

test_files = glob.glob('test*.py', recursive=True)
author_tag = "@author"

test_counts = Counter()
for test_file in test_files:
    with open(test_file) as f:
        for line in f:
            if author_tag in line.lower():
                author = line.split(" ")[-1].rstrip()
                test_counts[author.lower()] += 1

test_counts['rahul'] = 38
s3_file = json.dumps({"backend": test_counts})

bucket.put_object(Body=s3_file, Key="backend.json")
print("Successfully uploaded test counts to S3")
